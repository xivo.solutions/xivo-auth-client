requests==2.28.1  # from docker-compose
pyhamcrest==2.0.3
stevedore==4.0.2
git+https://gitlab.com/xivo.solutions/xivo-lib-rest-client.git
future==0.18.2
